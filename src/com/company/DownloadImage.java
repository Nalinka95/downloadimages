package com.company;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.concurrent.atomic.AtomicInteger;

public class DownloadImage implements Runnable {
    private String stringUrl;
    private static AtomicInteger nameId = new AtomicInteger(0);

    public DownloadImage(String stringUrl) {
        this.stringUrl = stringUrl;
    }

    public void run() {
        try (InputStream in = new URL(stringUrl).openStream()) {
            ArrayList<String> partsOfUrl = new ArrayList<>(Arrays.asList(stringUrl.split("\\.")));
            String extension = partsOfUrl.get(partsOfUrl.size() - 1);
            partsOfUrl.addAll(Arrays.asList(partsOfUrl.get(partsOfUrl.size() - 2).split("/")));
            String nameOfImage = partsOfUrl.get(partsOfUrl.size() - 1);
            if (extension.equalsIgnoreCase("png")) {
                String namePart = "/home/heshan/images/" + nameOfImage + "-" + nameId.incrementAndGet() + "-" + Math.random() + ".png";
                Files.copy(in, Paths.get(namePart));
            } else if (extension.equalsIgnoreCase("jpg")) {
                String namePart = "/home/heshan/images/" + nameOfImage + "-" + nameId.incrementAndGet() + "-" + Math.random() + ".jpg";
                Files.copy(in, Paths.get(namePart));
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
