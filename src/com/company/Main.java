package com.company;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class Main {

    public static void main(String[] args) {
        // write your code here
        ThreadPoolExecutor executor = (ThreadPoolExecutor) Executors.newCachedThreadPool();
        List<String> urls = new ArrayList<>();
        urls.add("https://homepages.cae.wisc.edu/~ece533/images/airplane.png");
        urls.add("https://homepages.cae.wisc.edu/~ece533/images/baboon.png");
        urls.add("https://homepages.cae.wisc.edu/~ece533/images/arctichare.png");
        urls.add("https://homepages.cae.wisc.edu/~ece533/images/boat.png");
        urls.add("https://homepages.cae.wisc.edu/~ece533/images/cat.png");
        urls.add("https://homepages.cae.wisc.edu/~ece533/images/fruits.png");
        urls.add("https://homepages.cae.wisc.edu/~ece533/images/frymire.png");
        urls.add("https://homepages.cae.wisc.edu/~ece533/images/girl.png");
        urls.add("https://homepages.cae.wisc.edu/~ece533/images/monarch.png");
        urls.add("https://homepages.cae.wisc.edu/~ece533/images/peppers.png");

        System.out.println("Downloading....");

        for (String url : urls) {
            DownloadImage image = new DownloadImage(url);
            executor.execute(image);
        }
        executor.shutdown();

        try {
            if (executor.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS)) {
                System.out.println("downloading is completed");
            }

        } catch (InterruptedException e) {
            System.out.println("it is finished");
        }
    }

}

